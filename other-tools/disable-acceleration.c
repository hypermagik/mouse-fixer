#include <stdio.h>
#include <string.h>
#include <IOKit/hidsystem/IOHIDLib.h>
#include <IOKit/hidsystem/IOHIDParameter.h>

int main(int argc, char **argv) {
    io_connect_t handle = NXOpenEventStatus();

    const int32_t accel = -1;
    IOHIDSetParameter(handle, CFSTR(kIOHIDMouseAccelerationType), &accel, sizeof accel);
    IOHIDSetParameter(handle, CFSTR(kIOHIDMouseScrollAccelerationKey), &accel, sizeof accel);

    NXCloseEventStatus(handle);
    return 0;
}
