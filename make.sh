#!/bin/zsh

clang++ --std=c++17 -framework IOKit -framework Cocoa -framework QuartzCore mouse-fixer.mm -o mouse-fixer
codesign -s - --entitlements mouse-fixer.entitlements --force mouse-fixer

